export const randomNumber = (max, min, decimal = 2) => {
    return +(Math.random() * (max - min) + min).toFixed();
}