import React, { Component } from 'react';
import SingleSeat from './SingleSeat';
import Legends from './Legends';
import movieSeat from '../icons/movie_seat.png';
import busSeat from '../icons/bus_seat.png';

class SeatLayout extends Component {

    state = {
        selectedSeats: [],
        unavailableSeats: ["0-1", "0-4", "0-16", "0-17", "1-10", "1-11", "3-4"],
        bookedSeats: ["1-2", "2-8", "3-1", "3-12", "3-13", "3-14"],
        type: busSeat
    }

    render() {
        const seatPosition = this.props.seatPosition;
        return (
            <React.Fragment>
                <div className="seat-container">
                    {this.generateLayout(seatPosition)}
                </div>
                <Legends seatIcon={this.state.type} />
            </React.Fragment>
        )
    }

    generateLayout = (seatPosition) => {
        return seatPosition.map((item, idx) => {
            return (
                <div key={idx} className="row">
                    {this.generateSeat(item, idx)}
                </div>
            );
        });
    };

    generateSeat = (seats, rowIdx) => {
        return seats.map((seat, idx) => {
            const pos = `${rowIdx}-${idx}`;
            const cls = this.getSeatClass(pos);
            if (seat) {
                return <SingleSeat key={idx} pos={pos} cls={cls} seatIcon={this.state.type} toggleSeatSelection={this.toggleSeatSelection} />;
            } else {
                return <SingleSeat key={idx} pos={pos} isSeatHidden={true} />;
            }
        });
    };

    getSeatClass = (pos) => {
        const { selectedSeats, unavailableSeats, bookedSeats } = this.state;
        if (unavailableSeats.includes(pos)) {
            return 'is-unavailable';
        } else if (bookedSeats.includes(pos)) {
            return 'is-booked';
        } else if (selectedSeats.includes(pos)) {
            return 'is-selected';
        } else {
            return '';
        }
    }

    toggleSeatSelection = (pos) => {
        let { selectedSeats } = this.state;
        if (!selectedSeats.includes(pos)) {
            selectedSeats.push(pos);
        } else {
            selectedSeats = selectedSeats.filter(seat => seat !== pos);
        }
        this.setState({
            selectedSeats
        })
    }

}

export default SeatLayout;