import React, { Component } from 'react';

class Legends extends Component {
    render() {
        const seatIcon = this.props.seatIcon;
        return (
            <div className="row">
                <div className="seat seat--legend ">
                    <img src={seatIcon} alt="Available seat" />
                    Available
                </div>
                <div className="seat seat--legend  is-unavailable">
                    <img src={seatIcon} alt="Unavailable seat" />
                    Unavaialble
                </div>
                <div className="seat seat--legend  is-booked">
                    <img src={seatIcon} alt="Booked seat" />
                    Booked
                </div>
                <div className="seat seat--legend  is-selected">
                    <img src={seatIcon} alt="Selected seat" />
                    Selected
                </div>
            </div>
        )
    }
}

export default Legends;