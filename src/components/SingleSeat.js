import React, { Component } from 'react';

class SingleSeat extends Component {
    render() {
        const { isSeatHidden, pos } = this.props;
        if (isSeatHidden) {
            return (<div className="seat" data-pos={pos}></div>);
        } else {
            return (<div className={`seat ${this.props.cls}`} data-pos={pos}> <img src={this.props.seatIcon} onClick={this.handleClick} alt="Select seat" /> </div>);
        }
    }

    handleClick = () => {
        this.props.toggleSeatSelection(this.props.pos);
    }

}

export default SingleSeat;