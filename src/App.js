import React, { Component } from 'react';
import SeatLayout from './components/SeatLayout';
import './App.css';

class App extends Component {
  seatPosition = [
    [1, 2, undefined, 3, 4, 5, 6, 7, 8, 9, 10, undefined, undefined, 11, 12, 13, 14, 15],
    [undefined, 1, 2, 3, undefined, undefined, undefined, 4, 5, 6, 7, 8, 9, 10, 11, undefined, undefined, 15],
    [1, 2, undefined, undefined, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    [undefined, 2, undefined, undefined, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
  ];

  render() {
    return (
      <React.Fragment>
        <h3 className="text-center">Select your seat!</h3>
        <SeatLayout seatPosition={this.seatPosition} />
      </React.Fragment >
    );
  }

}

export default App;
